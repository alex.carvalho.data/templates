# <img src="img/sample.png" alt="sample" width="30" style="vertical-align: middle;"> | Project title  

Here is brief, 1 liner, description of the project.  

<img src="img/terraform-logo.png" alt="terraform" width="90" style="vertical-align: middle;"> 
<img src="img/docker.png" alt="docker" width="90" style="vertical-align: middle;">
<img src="img/python.png" alt="python" width="90" style="vertical-align: middle;">

## Quick summary  

A more verbose overview of the project.

## Table of contents

- [Installation](#installation)
- [Usage](#usage)
- [How to run tests](#how-to-run-tests)
- [Contributing](#contributing)
- [Contact](#contact)

## Installation

### Installation dependencies

Required installed software:  
<sup>(not covered in these instructions)</sup>  

- example python 3.12+
- docker 26.1.3+
- poetry 1.8+

### Installation steps

#### 1. clone this repository

```bash
git clone git@gitlab.com:alex-carvalho-data-learn/rocksdb-py4j.git
```

#### 2. Install project defined dependencies

```bash
poetry install
```

## Usage

To run the project, use the following command:

```bash
python somthing.py
```

## How to run tests

To execute the tests, execute the following command:  

```
pytest
```

## Contributing

1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`.
3. Make your changes.
4. Push your branch: `git push origin feature-name`.
5. Create a pull request.

## Contact

alex carvalho - [linkedin.com/in/alex-carvalho-big-data](https://www.linkedin.com/in/alex-carvalho-big-data/)

