# Changelog

## [2.0.0] - 2020-07-23

_If you are upgrading: please see [`UPGRADING.md`](UPGRADING.md)._

### Added

- Support of CentOS
- `write()` method
- Documentation for the `read()` method

### Changed

- things

### Removed

- **Breaking:** remove `write()` method from public API (`01e3a64`)



## [1.0.1] - 2024-05-22

### Fixed

- Prevent segmentation fault upon `close()`



## [1.0.0] - 2024-05-21

### Added

- Readme template
- Changelog template 



## [0.1.0] - 2024-05-20

_Initial release_

[0.1.0]: https://github.com/owner/name/releases/tag/v0.1.0
[1.0.0]: https://github.com/owner/name/releases/tag/v1.0.0
[1.0.1]: https://github.com/owner/name/releases/tag/v1.0.1
[2.0.0]: https://github.com/owner/name/releases/tag/v2.0.0