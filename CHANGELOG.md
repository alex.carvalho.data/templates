# Changelog

## [1.0.1] - 2024-05-26  

### Changed  

- Make the Contact section more compact at README template  


## [1.0.0] - 2024-05-26

_Initial release_

### Added

- Readme template
- Changelog template 

[1.0.0]: https://gitlab.com/alex.carvalho.data/templates/-/tags/v1.0.0
[1.0.1]: https://gitlab.com/alex.carvalho.data/templates/-/tags/v1.0.1

