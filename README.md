# <img src="img/sample.png" alt="sample" width="30" style="vertical-align: middle;"> | Templates Project

This project stores sample templates for common things like README.md and CHANGELOG.md files.

## Table of contents

- [Templates](#templates)
- [Usage](#usage)
- [Contributing](#contributing)
- [Contact](#contact)

## Templates

Templates availables:

- README

## Usage

Every folder on the root directory is a template folder. (except for the /img folder).  
Most os them should be just to copy and past on the target project. However, a README.md inside every template can contains instructions for doing otherwise.  

## Contributing

1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`.
3. Make your changes.
4. Push your branch: `git push origin feature-name`.
5. Create a pull request.

## Contact

alex carvalho - [linkedin.com/in/alex-carvalho-big-data](https://www.linkedin.com/in/alex-carvalho-big-data/)

